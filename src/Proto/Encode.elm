module Proto.Encode exposing
    ( Encoder
    , encode
    )

import Bytes exposing (Bytes)
import Proto exposing (Field(..))


type Encoder
    = VarInt Int
    | LengthDelimited Bytes
    | Bit32 Int


encode : Encoder -> Field
encode encoder =
    Bl 0 False
