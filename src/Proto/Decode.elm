module Proto.Decode exposing
    ( Decoder
    , decode
    )

import Bytes exposing (Bytes)
import Proto exposing (Field(..))


type Decoder
    = VarInt Int
    | LengthDelimited Bytes
    | Bit32 Int


decode : Decoder -> Field
decode decoder =
    Bl 0 False
