module Proto exposing (Field(..))

import Bytes exposing (Bytes)


type Field
    = Int32 Int Int
    | UInt32 Int Int
    | SInt32 Int Int
    | Bl Int Bool
    | Enum Int Int
    | Str Int Bytes
    | Bt Int Bytes
    | Msg Int Bytes
    | Repeated Int Bytes
    | Fixed32 Int Int
    | SFixed32 Int Int
    | F Int Float
